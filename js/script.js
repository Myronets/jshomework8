// Задание
// Создать поле для ввода цены с валидацией.
//
//     Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.

const getPrice = document.createElement('input');
getPrice.type = 'text';
getPrice.placeholder = 'Price';
document.body.appendChild(getPrice);


getPrice.addEventListener('focus', ()=>{
    getPrice.style.border = '2px solid green';
    getPrice.style.outline = 'none';
});

getPrice.addEventListener('blur', ()=>{
    getPrice.style = '';
    if (getPrice.value >= 0) {
        getPrice.style = "color: green";

        const box = document.createElement('div');
        box.style = 'height: 20px; width: 300px; border: 1px solid grey; border-radius: 8px; margin-bottom: 10px; display: flex; justify-content: space-between; padding: 2px 6px';
        document.body.insertBefore(box, getPrice);

        const boxPrice = document.createElement('span');
        boxPrice.innerHTML = `Текущая цена: ${getPrice.value}`;

        const boxClose = document.createElement('span');
        boxClose.innerHTML = 'x';
        boxClose.style = 'border: 1px solid grey; border-radius: 50%; padding: 0px 3px; color: gray';

        box.appendChild(boxPrice);
        box.appendChild(boxClose);

        boxClose.addEventListener('click', () => {
            document.body.removeChild(box);
            getPrice.value = '';
        })
    } else {
        getPrice.style = 'border-color: red';
        const errorMsg = document.createElement('div');
        errorMsg.innerHTML = 'Please enter correct price';
        document.body.appendChild(errorMsg);
    }
});



